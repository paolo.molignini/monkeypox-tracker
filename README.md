# Monkeypox tracker dash app

This repository contains a dash app that visualizes global infection data on the monkeypox epidemic from May 2022 onwards. A detailed description of the app and its functionalities, including how to deploy it on Heroku for free, can be found in this collection of Medium articles: XXXXXXXXXXXXXXXX. 

Note that this repository is *not* associated with a Heroku account and does not contain files needed for deployment such as Procfile and requirements.txt. If you want to deploy this app yourself, please follow the aforementioned tutorial on Medium. 

I hope you will find this app a useful testing ground if you want to explore the power of plotly/dash for data visualization! Any feedback is welcome, but please bear in mind that this app is not actively updated. If you want to maintain or expand on this project, feel free to fork it and work on it further.

## System requirements:

You need to have python 3.7 or higher installed on your system for this app to run. Furthermore, the app requires the following packages:

numpy 1.21.0

pandas 1.3.1

plotly 5.8.0

dash 2.4.1

dash_bootstrap_components 1.1.0

The app was optimised for the package versions listed above. We do not guarantee that it will work with different versions.

## Structure of the repository:

The repository contains several files and folder:
- app.py is the main python script for the dash app.
- the folder modules contains additional python modules used to extract the data, create graphical objects, define the layout etc:
    - input.py: contains the input variables for the app.
    - extractor.py: contains functions to extract and manipulate the data from the .csv files.
    - fit.py: contains a function that performs an exponential fit to the data.
    - plots.py: contains the definition of the graphical objects shown in the app.
    - layout.py: contains the app layout.
- the folder assets contains .csv files with the raw data to visualize. The .csv files are organized by date. Note that not all dates are represented in the data! Each .csv file contains several columns with information for each country, listed alphabetically.

## Disclaimer

This app is intended for recreational purposes only and its main scope is to illustrate how to use plotly/dash packages. It does not contain exhaustive epidemiological data about monkeypox. It should not be used for any kind of medical, epidemiological, or healthcare policy evaluation. Please refer to official information and guidelines about monkeypox, e.g. from the [World Health Organization](https://www.who.int/news-room/fact-sheets/detail/monkeypox).
