############################################################################################
# IMPORTS
############################################################################################
# Dash:
import dash
from dash import Input, Output, State, callback
import dash_bootstrap_components as dbc

# Pandas for dataframe manipulation:
import pandas as pd

# Datetime for manipulating dates:
import datetime as dt
from datetime import date

# Custom modules:
from modules.input import *
from modules.extractor import data_extractor
from modules.fit import data_fit
from modules.plots import get_default_layout, get_worldmap_fig, get_tot_cases_fig
from modules.layout import get_layout
############################################################################################


############################################################################################
# DATA GENERATION:
############################################################################################

# Data source for today:
df_today = pd.read_csv(f'assets/cases-{current_date_tag}.csv')
country_list_available = df_today[df_today['CASES'] != 0]['COUNTRY']

# Extract data:
current_df, cases_df = data_extractor(start_date, end_date, country_list)
cases_fit_df = data_fit(country_list, initial_day_fit)

# Generate figures:
worldmap_fig = get_worldmap_fig(current_df)
tot_cases_fig = get_tot_cases_fig(cases_fit_df, country_list)
############################################################################################



############################################################################################
# SERVER INITIALIZATION:
############################################################################################
app = dash.Dash(__name__, prevent_initial_callbacks=True, external_stylesheets=[dbc.themes.BOOTSTRAP])

server = app.server

app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>Monkeypox Dashboard</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            <script type="text/javascript" src="https://cdnjs.buymeacoffee.com/1.0.0/button.prod.min.js" data-name="bmc-button" data-slug="pmolignini" data-color="#FFDD00" data-emoji="" data-font="Cookie" data-text="Buy me a coffee" data-outline-color="#000000" data-font-color="#000000" data-coffee-color="#ffffff" ></script>
            {%renderer%}
        </footer>
    </body>
</html>
'''
############################################################################################


############################################################################################
# LAYOUT
############################################################################################
app.layout = get_layout(today=today,
                        disabled_days=disabled_days,
                        worldmap_fig=worldmap_fig,
                        country_list_available=country_list_available,
                        tot_cases_fig=tot_cases_fig)
############################################################################################


############################################################################################
# CALLBACKS
############################################################################################
@app.callback(
    Output('graph-worldmap', 'figure'),
    Output('graph-tot-cases', 'figure'),
    Output('first-text', 'children'),
    Input('date-picker-single', 'date'),
    Input('country-list-toggle', 'n_clicks'),
    State('country-list-menu', 'value'),
    State('fit-start', 'value'),
    prevent_initial_call = True)
def update_data(updated_current_date_str, n_clicks, country_list, initial_day_fit_str):

    if not updated_current_date_str:
        raise PreventUpdate
    
    # Getting the dates in the correct format:
    initial_day_fit = int(initial_day_fit_str)
    updated_end_date = dt.datetime.strptime(updated_current_date_str, '%Y-%m-%d').date()
    
    # Updating the dataframes:
    updated_current_df, updated_cases_df = data_extractor(start_date, updated_end_date, country_list)
    updated_cases_fit_df = data_fit(country_list, initial_day_fit)
    
    # Updating the figures and the headline:
    updated_worldmap_fig = get_worldmap_fig(updated_current_df)
    updated_tot_cases_fig = get_tot_cases_fig(updated_cases_fit_df, country_list)
    updated_first_text = f"Confirmed non-endemic monkeypox cases as of {updated_current_date_str}"
    
    return updated_worldmap_fig, updated_tot_cases_fig, updated_first_text
############################################################################################





############################################################################################
if __name__ == '__main__':
    app.run_server(debug=True, use_reloader=False, dev_tools_hot_reload=False)
############################################################################################
