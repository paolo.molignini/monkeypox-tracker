############################################################################################
# IMPORTS
############################################################################################
# Plotly graphical objects to visualize
import plotly.graph_objects as go

# Pandas for dataframe manipulation
import pandas as pd

# Numpy for numerical calculations
import numpy as np

# Custom modules:
from modules.input import *
############################################################################################


############################################################################################
def get_default_layout(**kwargs) -> dict:

    layout_dict = {}
    layout_dict["margin"] = dict(t=30, l=30, r=30, b=30)

    if kwargs.get("show_xlabel", False) is True:
        layout_dict["xaxis"] = dict(title="Date and Time", showgrid=True, mirror=True)

    layout_dict["yaxis"] = dict(title="cases", showgrid=True, mirror=True)

    layout_dict["template"] = "plotly_dark"  # change the layout to a template if needed

    return layout_dict
############################################################################################


############################################################################################
# Plot of the world map:
############################################################################################
def get_worldmap_fig(current_df):

    
    pd.set_option('display.max_rows', None)
    print(current_df)
    
    worldmap_fig = go.Figure(data=go.Choropleth(
        locations = current_df['CODE'],
        z = current_df['CASES'],
        text = current_df['COUNTRY'],
        colorscale = cmap,
        autocolorscale=False,
        reversescale=False,
        marker_line_color='darkgray',
        marker_line_width=0.5,
        #colorbar_tickprefix = '$',
        colorbar_title = 'Cases',
    ))

    worldmap_fig.update_layout(
        width=700,
        height=500,
        geo=dict(bgcolor = 'rgb(11,11,11)',
            showframe=False,
            showcoastlines=False,
            projection_type='equirectangular'
        ),
        plot_bgcolor='rgb(11,11,11)',
        paper_bgcolor='rgb(11,11,11)',
        font_color='white',
    )
    return worldmap_fig
############################################################################################


############################################################################################
# Plot of the total cases per day:
############################################################################################
def get_tot_cases_fig(cases_df, country_list):
    
    # Initialization of layout and figure:
    layout = get_default_layout()
    tot_cases_fig = go.Figure(layout=layout)
    
    # Then construct the graphical object to plot.
    tot_cases_fig.add_trace(
        go.Scatter(
            x=cases_df['DATE'],
            y=cases_df['TOT CASES'],
            fill=None,
            mode="lines",
            line = dict(color = 'firebrick', width = 6),
            name='Total cumulative cases',
        )
    )
    
    tot_cases_fig.add_trace(
        go.Scatter(
            x=cases_df['DATE'],
            y=cases_df['TOT CASES_fit'],
            fill=None,
            mode="lines",
            line = dict(color = 'firebrick', width = 6, dash='dash'),
            name='Total cumulative cases (exp fit)',
        )
    )
    
    # Add each country separately:
    for i,country in enumerate(country_list):
        tot_cases_fig.add_trace(
            go.Scatter(
                x=cases_df['DATE'],
                y=cases_df[country],
                fill=None,
                mode="lines",
                line = dict(color = country_colors[i], width = 4),
                name=str(country),
            )
        )
    
    
    # Add here extrapolated curves:
    for i,country in enumerate(country_list):
        tot_cases_fig.add_trace(
            go.Scatter(
                x=cases_df['DATE'],
                y=cases_df[country+"_fit"],
                fill=None,
                mode="lines",
                line = dict(color = country_colors[i], width = 4, dash='dash'),
                name=str(country)+" (exp fit)",
            )
        )
    
    
    
    tot_cases_fig.update_layout(
        width=700,
        height=500,
        showlegend = True,
        legend = dict(
                      x=0,
                      y=.95,
                      traceorder="normal",
                      font=dict(
                               family="sans-serif",
                               size=16,
                               color="white"
                               )
                    )
        )
        
    return tot_cases_fig
############################################################################################
