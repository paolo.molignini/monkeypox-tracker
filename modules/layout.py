############################################################################################
# IMPORTS
############################################################################################
# Dash:
from dash import html, dcc
import dash_bootstrap_components as dbc

# Datetime for manipulating dates:
import datetime as dt

# Custom modules:
from modules.input import *
############################################################################################


############################################################################################
# LAYOUT
############################################################################################
def get_layout(today, disabled_days, worldmap_fig, country_list_available, tot_cases_fig):

    layout = html.Div(style={'backgroundColor': colors['background']},
        children=[
        html.Br(style={'marginBottom': '1px'}),
        html.Center(html.H2(f"Confirmed monkeypox cases as of {current_date_str}",
                    id='first-text',
                    style = {'color': 'white'})),
        html.Br(style={'marginBottom': '20px'}),
        html.Div(dbc.Row(
            [
                dbc.Col(
                    html.H5("Choose a date:",
                        id='date-text',
                        style = {'color': 'white'}
                        ),
                    width=1,
                    align="right",
                    ),
                dbc.Col(
                    dcc.DatePickerSingle(
                        id='date-picker-single',
                        min_date_allowed=dt.date(2022, 5, 7),
                        max_date_allowed=today,
                        initial_visible_month=dt.date(2022, 5, 7),
                        date=today,
                        disabled_days=disabled_days,
                        style={'display': 'inline-block'}
                        ),
                    width=1,
                    ),
            ],
            justify="center",
            className="g-0",
            )),
        html.Br(style={'marginBottom': '20px'}),
        html.Center(dcc.Graph(id='graph-worldmap', figure=worldmap_fig)),
        html.Br(style={'marginBottom': '20px'}),
        html.Div(dbc.Row(
            [
                dbc.Col(
                    html.H5("Choose countries:",
                        id='country-picker-text',
                        style = {'color': 'white'}
                        ),
                    width=1,
                    align="right",
                    ),
                dbc.Col(
                    html.Div(
                        html.Div(
                            children=dbc.DropdownMenu(
                                children=[
                                    dcc.Checklist(
                                        options=country_list_available,
                                        value=country_list,
                                        labelStyle={"display": "block"},
                                        style={
                                            "height": 400,
                                            "width": 200,
                                            "overflow": "auto",
                                            },
                                        id='country-list-menu',
                                        ),
                                    ],
                                    label="country-list-menu",
                                ),
                            ),
                        id='country-list-div',
                    ),
                    width=3,
                    ),
                dbc.Col(
                html.H5("Days after which to start fitting:",
                    id='dropdown-fit-text',
                    style = {'color': 'white'}
                    ),
                width=1,
                align="right",
                ),
                dbc.Col(
                    html.Div(
                        dcc.Dropdown(
                            ['0', '10', '20', '30'], '0', id='fit-start',
                            ),
                        style={"width": "50%"},
                        ),
                    width=3,
                ),
                dbc.Col(
                    html.Button(
                        "Update",
                        id='country-list-toggle',
                        n_clicks=0,
                    ),
                    width=3,
                    align="right",
                    ),
                ],
            justify="center",
            className="g-0",
            )),
        html.Br(style={'marginBottom': '5px'}),
        html.Center(dcc.Graph(id='graph-tot-cases', figure=tot_cases_fig)),
        html.Br(style={'marginBottom': '20px'}),
        html.H3("  Sources:",
            id='sources-text',
            style = {'color': 'white', 'margin-left': '15px'}),
        html.Br(style={'marginBottom': '5px'}),
        html.Div([
            html.P("25/5/2022:",
                id='sources-text-25-5-2022',
                style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
            html.A(
                "[1]",
                href='https://www.reuters.com/business/healthcare-pharmaceuticals/monkeypox-cases-around-world-2022-05-23/',
                target="_blank")
            ], style = {'display' : 'flex'}),
        html.Div([
            html.P("26/5/2022:",
                id='sources-text-26-5-2022',
                style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
            html.A(
                "[1]",
                href='https://news.sky.com/story/monkeypox-first-infection-detected-in-wales-with-more-uk-cases-to-be-revealed-later-12621670',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[2]",
                href='https://edition.cnn.com/2022/05/26/health/monkeypox-cdc-update/index.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[3]",
                href='https://www.ctvnews.ca/health/monkeypox-cases-up-to-26-first-case-detected-in-ontario-phac-1.5920070',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[4]",
                href='https://www.thelocal.it/20220525/italian-monkeypox-cases-rise-to-seven/',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[5]",
                href='https://www.theportugalnews.com/news/2022-05-26/confirmed-cases-of-monkeypox-up-to-58/67402',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[6]",
                href='https://www.reuters.com/world/europe/spains-monkeypox-case-tally-rises-84-health-ministry-says-2022-05-26/',
                target="_blank",
                style = {'margin-right': '2px'}),
            ], style = {'display' : 'flex'}),
        html.Div([
            html.P("27/5/2022:",
                    id='sources-text-27-5-2022',
                    style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
            html.A(
                "[1]",
                href=            'https://news.sky.com/story/sixteen-further-monkeypox-cases-detected-in-england-uk-health-security-agency-confirms-12622587',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[2]",
                href='https://www.nbcnews.com/health/health-news/us-monkeypox-cases-identified-rcna30388',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[3]",
                href='https://www.euractiv.com/section/politics/short_news/sweden-confirms-second-monkeypox-case/',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[4]",
                href='https://www.lefigaro.fr/economie/variole-du-singe-5-cas-en-france-assez-de-vaccins-pour-les-cas-contact-dit-brigitte-bourguignon-20220525',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[5]",
                href='https://www.reuters.com/world/europe/italys-monkeypox-cases-rise-12-one-more-suspected-health-ministry-2022-05-27/',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[6]",
                href='https://www.washingtonpost.com/world/argentina-reports-case-of-monkeypox-man-traveled-from-spain/2022/05/27/9064d2f4-ddc2-11ec-bc35-a91d0a94923b_story.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[7]",
                href='https://www.lavanguardia.com/vida/20220527/8297933/100-casos-espana-viruela-mono.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[8]",
                href='https://www.swissinfo.ch/spa/viruela-mono-portugal_viruela-del-mono-se-dispara-en-portugal-con-74-casos/47628432',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[9]",
                href='https://www.blick.ch/schweiz/ueber-200-faelle-ausserhalb-afrikas-bestaetigt-dritter-affenpocken-fall-der-schweiz-in-zuerich-gemeldet-id17525779.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[10]",
                href='https://www.tagesspiegel.de/berlin/insgesamt-16-faelle-in-deutschland-acht-bestaetigte-affenpocken-faelle-in-berlin/28380382.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            html.A(
                "[11]",
                href='https://www.dailymail.co.uk/health/article-10861715/CDC-panel-recommends-monkeypox-vaccine-lab-workers-health-responders-health-care-workers.html',
                target="_blank",
                style = {'margin-right': '2px'}),
            ], style = {'display' : 'flex'}),
        html.Div([
            html.P("30/5/2022:",
                        id='sources-text-30-5-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
            html.A(
                    "[1]",
                    href='https://www.theguardian.com/world/2022/may/30/further-71-cases-of-monkeypox-detected-in-england',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[2]",
                    href='https://www.reuters.com/business/healthcare-pharmaceuticals/monkeypox-cases-rise-spain-120-portugal-96-2022-05-30/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[3]",
                    href='https://nltimes.nl/2022/05/30/dutch-monkeypox-cases-double-26-says-rivm',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[4]",
                    href='https://www.connexionfrance.com/article/French-news/Monkeypox-in-France-16-cases-confirmed-first-vaccinations-received',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[5]",
                    href='https://www.n-tv.de/wissen/Vieles-am-Affenpocken-Ausbruch-bleibt-raetselhaft-article23364254.html',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[6]",
                    href='https://www.dailymail.co.uk/health/article-10868441/First-potential-human-human-transmission-monkeypox-detected-United-States.html',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[7]",
                    href='https://www.maltatoday.com.mt/news/national/117006/first_monkeypox_case_detected_in_malta#.YpU4xC1Q1n4',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[8]",
                    href='https://www.timesofisrael.com/liveblog_entry/second-monkeypox-case-identified-in-israel-report/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[9]",
                    href='https://english.alaraby.co.uk/news/uae-records-three-more-monkeypox-cases',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[10]",
                    href='https://www.reuters.com/world/americas/mexico-confirms-first-case-monkeypox-health-official-2022-05-28/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[11]",
                    href='https://bnonews.com/monkeypox/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            ], style = {'display' : 'flex'}),
        html.Div([
            html.P("31/5/2022:",
                        id='sources-text-31-5-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
            html.A(
                    "[1]",
                    href='https://www.folkhalsomyndigheten.se/smittskydd-beredskap/utbrott/aktuella-utbrott/apkoppor-europa-nordamerika-oceanien-maj-2022-/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[2]",
                    href='https://www.reuters.com/world/europe/hungary-reports-first-case-monkeypox-2022-05-31/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[3]",
                    href='https://www.breakingnews.ie/ireland/second-monkeypox-cases-confirmed-in-ireland-1313406.html',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[4]",
                    href='http://outbreaknewstoday.com/two-cases-of-monkeypox-in-denmark-ssi-risk-assessment-36709/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[5]",
                    href='https://www.hln.be/privacy-gate/accept-tcf2?redirectUri=%2Fwetenschap%2Flaboratorium-van-marc-van-ranst-ku-leuven-ontdekt-handvol-mutaties-in-apenpokkenvirus%7Ea36f3bed%2F&authId=f661ea6a-6f82-4998-a459-5a5df5e67e2c',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[6]",
                    href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-monkeypox-point-de-situation-au-30-mai-2022',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[7]",
                    href='https://docs.google.com/spreadsheets/d/1p-_ufMx6F5ol32MFfes2vPVYzKWYiovxgCnHxEIpQOE/edit#gid=0',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[8]",
                    href='https://www.publico.pt/2022/05/31/ciencia/noticia/varioladosmacacos-portugal-chega-100-casos-confirmados-2008345',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[9]",
                    href='https://www.abc.es/sociedad/abci-sanidad-confirma-132-casos-positivos-viruela-mono-espana-202205311455_noticia.html',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[10]",
                    href='https://www.gov.uk/government/news/monkeypox-cases-confirmed-in-england-latest-updates',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            html.A(
                    "[11]",
                    href='https://bnonews.com/monkeypox/',
                    target="_blank",
                    style = {'margin-right': '2px'}),
            ], style = {'display' : 'flex'}),
        html.Div([
                html.P("1/6/2022:",
                            id='sources-text-1-6-2022',
                            style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.axios.com/2022/06/01/portugal-spain-monkeypox-cases',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.merkur.de/deutschland/pandemie-affenpocken-faelle-deutschland-impfung-impfdosen-liefertermin-lauterbach-zr-91579359.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://o.canada.com/news/local-news/torontos-second-monkeypox-case-confirmed-public-health',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.ansa.it/friuliveneziagiulia/notizie/2022/05/31/vaiolo-scimmie-identificato-primo-caso-in-fvg_0a116657-92e0-48df-b7bd-861fa5eea19b.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.rtvslo.si/zdravje/v-sloveniji-potrdili-drugi-primer-okuzbe-z-virusom-opicjih-koz/628637',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.fhi.no/en/news/2022/one-person-with-confirmed-monkeypox-in-norway/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("2/6/2022:",
                        id='sources-text-2-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.chronicle.gi/gha-confirms-first-case-of-monkeypox-in-gibraltar/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.health.nsw.gov.au/news/Pages/20220602_00.aspx',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.hpsc.ie/a-z/zoonotic/monkeypox/title-22106-en.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.wam.ae/en/details/1395303053461',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.hln.be/binnenland/al-14-besmettingen-met-apenpokkenvirus-vastgesteld-in-belgie~a8bf4b7c/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.nbcchicago.com/news/local/chicago-reports-first-probable-case-of-monkeypox/2848170/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://www.ansa.it/lombardia/notizie/2022/05/31/vaiolo-scimmie-11-i-casi-in-lombardia_a4c062c0-0194-4003-99b2-6762af8fdbc8.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-monkeypox-point-de-situation-au-1er-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://www.rivm.nl/en/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.quebec.ca/sante/problemes-de-sante/a-z/variole-du-singe',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.publico.pt/2022/06/02/ciencia/noticia/varioladosmacacos-casos-confirmados-portugal-sobem-138-2008633?ref=saude&cx=page__content',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://www.abc.es/sociedad/abci-sanidad-confirma-156-casos-viruela-mono-14-mas-miercoles-202206021311_noticia.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://www.gov.uk/government/news/monkeypox-cases-confirmed-in-england-latest-updates',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[14]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),     
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("3/6/2022:",
                        id='sources-text-3-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.reuters.com/world/europe/latvia-records-first-case-monkeypox-2022-06-03/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.nrk.no/vestland/apekopper-pavist-i-bergen-1.15987724',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://thl.fi/en/web/thlfi-en/-/second-probable-case-of-monkeypox-in-hus-area-',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://english.sta.si/3044830/third-case-of-monkeypox-confirmed-in-slovenia?q=monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.health.vic.gov.au/health-alerts/health-warning-on-monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.folkhalsomyndigheten.se/smittskydd-beredskap/utbrott/aktuella-utbrott/apkoppor-europa-nordamerika-oceanien-maj-2022-/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://prazsky.denik.cz/zpravy_region/opici-nestovice-praha.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/affenpocken.html#573159143',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://www.hln.be/binnenland/intussen-al-zeventien-besmettingen-met-apenpokkenvirus-vastgesteld-in-belgie~af6e5fe7/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.ansa.it/trentino/notizie/2022/06/03/diagnosticato-primo-caso-di-vaiolo-delle-scimmie-in-trentino_22c15c11-e117-4363-a9b1-eeda484b0961.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-monkeypox-point-de-situation-au-3-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://www.fr.de/panorama/infektionen-news-affenpocken-virus-deutschland-welt-europa-who-rki-zahlen-faelle-zr-91583787.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://www.ndtv.com/world-news/monkeypox-monkeypox-in-us-us-cdc-on-monkeypox-monekypox-virus-3038094',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[14]",
                        href='https://www.publico.pt/2022/06/03/ciencia/noticia/varioladosmacacos-portugal-cinco-casos-confirmados-sao-ja-143-2008782',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[15]",
                        href='https://www.devdiscourse.com/article/health/2060482-spains-monkeypox-case-tally-rises-to-186-health-ministry-says',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[16]",
                        href='https://www.gov.uk/government/news/monkeypox-cases-confirmed-in-england-latest-updates',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[17]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("6/6/2022:",
                        id='sources-text-6-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.health.nsw.gov.au/news/Pages/20220604_00.aspx',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='http://www.bccdc.ca/about/news-stories/stories/2022/first-monkeypox-case',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://observador.pt/2022/06/06/dgs-confirma-mais-10-casos-de-variola-dos-macacos-num-total-de-153/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.rtve.es/noticias/20220606/viruela-mono-sanidad-casos-total-espana/2366485.shtml',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://news.sky.com/story/monkeypox-77-more-cases-detected-in-uk-taking-total-to-302-12628835',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("7/6/2022:",
                        id='sources-text-7-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/affenpocken.html#573159143',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.ausl.pc.it/news/1024/a-piacenza-accertati-tre-casi-di-monkeypox-collegati-tra-loro.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.rivm.nl/monkeypox-apenpokken',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-monkeypox-point-de-situation-au-7-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                        
                html.A(
                        "[5]",
                        href='https://www.publico.pt/2022/06/07/ciencia/noticia/varioladosmacacos-portugal-soma-13-infeccoes-sao-166-casos-confirmados-2009197',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("8/6/2022:",
                        id='sources-text-8-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.rtve.es/noticias/20220608/viruela-mono-sanidad-casos-total-espana/2370522.shtml',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.jn.pt/nacional/portugal-com-191-casos-de-variola-dos-macacos-14925475.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html;jsessionid=0FB3D1103449DE4FCC03AC160542D0F5.internet061?nn=2386228',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://globalnews.ca/news/8906135/monkeypox-travel-what-canadians-should-know/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://sante.journaldesfemmes.fr/fiches-maladies/2816653-variole-du-singe-monkeypox-cas-france-symptomes-bordeaux-bouton-origine-contagion/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.cdc.gov/poxvirus/monkeypox/response/2022/index.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://www.hln.be/binnenland/24-besmettingen-met-apenpokkenvirus-vastgesteld-in-belgie~a882859a/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.wam.ae/en/details/1395303055318',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/affenpocken.html#573159143',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.hpsc.ie/a-z/zoonotic/monkeypox/title-22106-en.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.who.int/emergencies/disease-outbreak-news/item/2022-DON390',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://twitter.com/tv3_ghana/status/1534544477481844736',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://sum.dk/nyheder/2022/juni/tre-danske-tilfaelde-af-abekopper',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[14]",
                        href='https://www.jpost.com/breaking-news/article-708782',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[15]",
                        href='https://www.kathimerini.gr/society/561901132/eylogia-ton-pithikon-to-proto-episimo-kroysma-stin-ellada-i-anakoinosi-toy-eody/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[16]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("9/6/2022:",
                        id='sources-text-9-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='http://www.ial.sp.gov.br/resources/editorinplace/ial/2022_3_23/reports1/report_mpxv.pdf',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://grapevine.is/news/2022/06/09/first-monkeypox-cases-diagnosed-in-iceland/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.ynetnews.com/article/sk0f00yjy5',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.health.vic.gov.au/health-alerts/health-warning-on-monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.ecdc.europa.eu/en/news-events/epidemiological-update-monkeypox-multi-country-outbreak-8-june',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.rivm.nl/monkeypox-apenpokken',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-monkeypox-point-de-situation-au-9-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://globalnews.ca/news/8910310/monkeypox-cases-canada-update-june-9/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://abcnews.go.com/Health/wireStory/german-panel-recommends-vaccines-exposure-monkeypox-85282918',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.publico.pt/2022/06/09/ciencia/noticia/varioladosmacacos-portugal-soma-18-casos-ultrapassa-200-infectados-2009490',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.dailymail.co.uk/news/article-10896545/UK-logs-NINETEEN-cases-monkeypox-virus-elevated-threat-level-plague.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://www.heraldo.es/noticias/nacional/2022/06/09/sanidad-eleva-casos-viruela-mono-detectados-espana-1580580.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("10/6/2022:",
                        id='sources-text-10-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.gov.uk/government/news/monkeypox-cases-confirmed-in-england-latest-updates',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.rtve.es/noticias/20220610/viruela-mono-sanidad-casos-total-espana/2374182.shtml',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html;jsessionid=79F297B9D42A8852B8ECC161996D49D1.internet061?nn=2386228',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://tvn24.pl/polska/malpia-ospa-w-polsce-jest-pierwszy-potwierdzony-przypadek-chory-jest-w-warszawie-5745721',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("14/6/2022:",
                        id='sources-text-14-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.voanews.com/a/uk-reports-more-cases-of-monkeypox-mostly-in-men/6615464.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.bild.de/ratgeber/gesundheit/gesundheit/affenpocken-mehr-als-200-faelle-in-deutschland-registriert-allesamt-maenner-80399908.bild.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.laverdad.es/murcia/viruela-mono-region-de-murciar-20220614144059-nt.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.cidrap.umn.edu/news-perspective/2022/06/uk-reports-194-more-monkeypox-cases-us-total-hits-49',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.tsf.pt/portugal/sociedade/numero-de-casos-de-variola-dos-macacos-em-portugal-sobe-para-231-14939403.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://bnonews.com/monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://www.ad.nl/buitenland/who-zoekt-nieuwe-naam-voor-apenpokken-deze-naam-is-discriminerend~aa108ceb/?referrer=https%3A%2F%2Fwww.google.ch%2F',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-variole-du-singe-point-de-situation-au-14-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://globalnews.ca/news/8919210/quebec-montreal-monkeypox-update/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='                    https://fr.metrotime.be/belgique/variole-du-singe-la-vaccination-recommandee-pour-les-personnes-non-immunisees-contre-la-variole-en-belgique',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.watson.ch/schweiz/gesundheit/337188723-fuenf-neue-affenpockenfaelle-ueber-das-wochenende',
                    target="_blank",
                    style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("15/6/2022:",
                        id='sources-text-15-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.walesonline.co.uk/news/uk-news/new-monkeypox-cases-england-wales-24232653',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/affenpocken.html#573159143',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.diariodesevilla.es/sociedad/Confirmados-casos-viruela-mono-Espana_0_1693032155.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.tsf.pt/portugal/sociedade/numero-de-casos-de-variola-dos-macacos-registados-em-portugal-sobe-para-241-14942385.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.canada.ca/en/public-health/services/diseases/monkeypox.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://fr.metrotime.be/belgique/cinquante-deux-belges-ont-attrape-la-variole-du-singe-voici-leur-profil',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.lanacion.com.ar/sociedad/viruela-del-mono-confirman-el-tercer-caso-del-virus-en-la-argentina-nid10062022/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.reuters.com/business/healthcare-pharmaceuticals/venezuela-confirms-first-case-monkeypox-2022-06-12/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("17/6/2022:",
                        id='sources-text-17-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://news.sky.com/story/uk-records-50-new-monkeypox-cases-taking-total-to-574-12635630',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.ctvnews.ca/health/168-cases-of-monkeypox-confirmed-in-canada-including-141-in-quebec-1.5951814',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.santepubliquefrance.fr/les-actualites/2022/cas-de-variole-du-singe-point-de-situation-au-16-juin-2022',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.quotidianosanita.it/studi-e-analisi/articolo.php?articolo_id=105581',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.skipr.nl/nieuws/bijna-honderd-gevallen-van-apenpokken-in-nederland/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://sicnoticias.pt/pais/variola-dos-macacos-portugal-com-276-casos-confirmados/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.huffingtonpost.es/entry/espana-roza-los-500-casos-confirmados-de-viruela-del-mono_es_62acc25de4b0c77098adfbe4',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://apa.at/news/sieben-weitere-affenpocken-faelle-in-oesterreich-2/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.poder360.com.br/saude/saude-confirma-6o-caso-de-variola-dos-macacos-no-brasil/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.reuters.com/business/healthcare-pharmaceuticals/serbia-reports-first-case-monkeypox-2022-06-17/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://www.hpsc.ie/a-z/zoonotic/monkeypox/title-22106-en.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://www.blick.ch/ausland/ich-dachte-es-sind-pickel-oder-stiche-so-schwer-sind-affenpocken-zu-erkennen-patient-schildert-verlauf-id17586392.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[14]",
                        href='https://www.blick.ch/ausland/ich-dachte-es-sind-pickel-oder-stiche-so-schwer-sind-affenpocken-zu-erkennen-patient-schildert-verlauf-id17586392.html',
                        target="_blank",
                        style = {'margin-right': '2px'}), 
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("20/6/2022:",
                        id='sources-text-20-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.aa.com.tr/en/health/lebanon-reports-1st-case-of-monkeypox/2618067',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://www.timesofisrael.com/israel-confirms-first-communal-spread-of-monkeypox/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.reuters.com/world/americas/chile-reports-first-case-monkeypox-2022-06-17/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://www.who.int/emergencies/disease-outbreak-news/item/2022-DON393',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://www.today.it/cronaca/vaiolo-scimmie-casi-italia.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://sol.sapo.pt/artigo/774098/portugal-com-quase-300-casos-de-variola-dos-macacos',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.watson.ch/schweiz/medizin/795925005-bund-vermeldet-neun-neue-affenpocken-faelle-seit-freitag',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://www.dailymail.co.uk/health/article-10934743/Indiana-Missouri-record-presumptive-cases-monkeypox.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("23/6/2022:",
                        id='sources-text-23-6-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://www.reuters.com/business/healthcare-pharmaceuticals/south-africa-confirms-first-monkeypox-case-health-minister-2022-06-23/',
                              target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[2]",
                        href='https://edition.cnn.com/2022/06/22/asia/monkeypox-singapore-south-korea-outbreak-virus-intl-hnk/index.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[3]",
                        href='https://www.lavenir.net/actu/societe/2022/06/22/variole-du-singe-77-personnes-infectees-identifiees-en-belgique-IQ5YUTX5SNC6LOIBBMUSFC5CNU/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[4]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[5]",
                        href='https://ooe.orf.at/stories/3161917/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[6]",
                        href='https://veja.abril.com.br/saude/sobe-para-11-o-numero-de-casos-de-variola-dos-macacos-no-brasil/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[7]",
                        href='https://globalnews.ca/news/8940549/monkeypox-canada-cases-june-22/',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[8]",
                        href='https://www.telesurtv.net/news/chile-tercer-caso-viruela-mono-20220622-0028.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[9]",
                        href='https://www.lefigaro.fr/flash-actu/variole-du-singe-277-cas-confirmes-en-france-un-premier-chez-une-femme-20220621',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[10]",
                        href='https://www.rki.de/DE/Content/InfAZ/A/Affenpocken/Ausbruch-2022-Situation-Deutschland.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[11]",
                        href='https://www.luxtimes.lu/en/luxembourg/first-case-of-monkeypox-detected-in-luxembourg-62aaebfede135b92360305db',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[12]",
                        href='https://www.hartvannederland.nl/nieuws/gezondheid/apenpokken-in-nederland-tot-nu-toe-167-gevallen',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[13]",
                        href='https://www.srf.ch/wissen/gesundheit/aktuelle-lage-affenpocken-sind-sexuell-uebertragbar-aber-nicht-nur',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[14]",
                        href='https://www.theguardian.com/world/2022/jun/21/monkeypox-vaccines-uk-cases-near-800',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[15]",
                        href='https://www.voanews.com/a/us-expanding-monkeypox-testing-/6629456.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[16]",
                        href='https://www.lavanguardia.com/vida/20220623/8360662/viruela-mono-supera-3-000-casos-contengan-contagios.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                html.A(
                        "[17]",
                        href='https://www.tsf.pt/portugal/sociedade/numero-de-casos-de-variola-dos-macacos-em-portugal-sobe-para-328-14956177.html',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
            html.Div([
                    html.P("26/6/2022:",
                            id='sources-text-26-6-2022',
                            style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                    html.A(
                            "[1]",
                            href='https://ourworldindata.org/monkeypox',
                            target="_blank",
                            style = {'margin-right': '2px'}),
                    ], style = {'display' : 'flex'}),
            html.Div([
                    html.P("1/7/2022:",
                            id='sources-text-1-7-2022',
                            style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                    html.A(
                            "[1]",
                            href='https://ourworldindata.org/monkeypox',
                            target="_blank",
                            style = {'margin-right': '2px'}),
                    ], style = {'display' : 'flex'}),
            html.Div([
                    html.P("5/7/2022:",
                            id='sources-text-5-7-2022',
                            style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                    html.A(
                            "[1]",
                            href='https://ourworldindata.org/monkeypox',
                            target="_blank",
                            style = {'margin-right': '2px'}),
                    ], style = {'display' : 'flex'}),
            html.Div([
                    html.P("11/7/2022:",
                            id='sources-text-11-7-2022',
                            style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                    html.A(
                            "[1]",
                            href='https://ourworldindata.org/monkeypox',
                            target="_blank",
                            style = {'margin-right': '2px'}),
                    ], style = {'display' : 'flex'}),
        html.Div([
                html.P("18/7/2022:",
                        id='sources-text-18-7-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("21/7/2022:",
                        id='sources-text-21-7-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("25/7/2022:",
                        id='sources-text-25-7-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("5/8/2022:",
                        id='sources-text-5-8-2022',
                        style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("12/8/2022:",
                    id='sources-text-12-8-2022',
                    style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Div([
                html.P("19/8/2022:",
                    id='sources-text-19-8-2022',
                    style = {'color': 'white', 'margin-left': '15px', 'margin-right': '5px'}),
                html.A(
                        "[1]",
                        href='https://ourworldindata.org/monkeypox',
                        target="_blank",
                        style = {'margin-right': '2px'}),
                ], style = {'display' : 'flex'}),
        html.Br(style={'marginBottom': '20px'}),
        ])
        
    return layout
#############################################################################
