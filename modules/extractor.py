############################################################################################
# IMPORTS
############################################################################################
# Pandas for dataframe manipulation:
import pandas as pd

# Custom modules:
from modules.input import *
############################################################################################


############################################################################################
# Data extraction and manipulation:
############################################################################################
def data_extractor(start_date, end_date, country_list):

    cases_df = pd.DataFrame()
    current_date = start_date
    
    while current_date <= end_date:
    
        current_date = current_date + date_delta
        date_tag = current_date.strftime('%d-%m-%Y')

        # get data source and count cases every day:
        try:
            daily_df = pd.read_csv(f'assets/cases-{date_tag}.csv')
            
            # Extract data for each country singularly:
            dict1 = {}
            for country in country_list:
                country_cases = daily_df.loc[daily_df['COUNTRY'] == country, 'CASES'].iloc[0]
                dict1[country] = country_cases
            # Extract data about all cases:
            dict2 = {'DATE': [current_date], 'TOT CASES': [daily_df['CASES'].sum()]}
            # Create dictionary and concatenate with previous data:
            tot_dict = {**dict1, **dict2}
            daily_data = pd.DataFrame.from_dict(tot_dict)
            
            print(daily_data)
            cases_df = pd.concat([cases_df, daily_data], ignore_index=True)
        except:
            print(f"No data for {current_date.strftime('%d-%m-%Y')}")
            continue

    cases_df.to_csv('assets/total-cases.csv')

    # Data source for today:
    current_date_tag = end_date.strftime('%d-%m-%Y')
    df_today = pd.read_csv(f'assets/cases-{current_date_tag}.csv')
    
    return df_today, cases_df
############################################################################################
