############################################################################################
# IMPORTS
############################################################################################
import pandas as pd
import numpy as np

# Custom modules:
from modules.input import *
############################################################################################

############################################################################################
# Data fit:
############################################################################################
def data_fit(country_list, initial_day_fit):

    # Get the raw data to fit:
    cases_df = pd.read_csv('assets/total-cases.csv')
        
    # For each country, extract x and y variable (date  and number of cases):
    for country in country_list:
        
        print(f"Fitting data for {country}")
        
        # extract x in days from the first date:
        x0 = pd.to_datetime(cases_df['DATE']).astype(int)/10**9
        x0 = (x0 - x0[0])/86400
        x0 = x0.to_numpy()
        # Neglect the first initial days:
        x = x0[initial_day_fit:]
        
        # extract y (cases)
        # Arbitrarily assign +1 to all cases. This is done to make log(y) well defined
        # (i.e. log(0) is ill-defined).
        y = cases_df[country].to_numpy() + 1
        y = y[initial_day_fit:]
        print(f"x is: {x}")
        print(f"y is: {y}")
        
        # Perform the fit of y = A*e^{B*x}.
        # First consider log(y) = log(A) + log(e^{B*x}) -> log(y) = log(A) + B*x.
        # We therefore do a linear fit of log(y) against x.
        # The slope gives B directly, while the intercept gives log(A).
        
        print(f"log(y) is: {np.log(y)}")
        pars = np.polyfit(x, np.log(y), 1)
        print(f"fitted parameters: {pars}")
        fitted_data = np.exp(pars[1])*np.exp(pars[0]*x0) - 1
        print(f"fitted data: {fitted_data}")

        # Add the fitted data as additional columns in the dataframe:
        cases_df[country+"_fit"] = fitted_data


    # do the fit of the total cases:
    y_tot = cases_df['TOT CASES'].to_numpy() + 1
    y_tot = y_tot[initial_day_fit:]
    pars_tot = np.polyfit(x, np.log(y_tot), 1)
    fitted_data_tot = np.exp(pars_tot[1])*np.exp(pars_tot[0]*x0) - 1
    cases_df['TOT CASES_fit'] = fitted_data_tot

    cases_df.to_csv('assets/total-cases.csv')


    return cases_df
############################################################################################
